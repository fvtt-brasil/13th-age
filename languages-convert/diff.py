# -*- coding: utf-8 -*-
#!/usr/bin/env python

# Requires PyYAML, deepdiff
# Baseado em: https://github.com/mhilbrunner/toolkit13-de/blob/main/languages-convert/diff.py

import yaml
from deepdiff import DeepDiff
import re

def yaml_as_dict(my_file):
    with open(my_file, "r", encoding="utf-8") as fp:
        return yaml.load(fp, Loader=yaml.FullLoader)

def format_key(k):
    k = k.removeprefix("root[")
    k = k.replace("[", ".")
    k = k.replace("'", "")
    k = k.replace("]", "")
    return k

def main():
    pt = yaml_as_dict("pt-BR.yaml")
    en = yaml_as_dict("en.yaml")
    diff = DeepDiff(pt, en, ignore_order=True, report_repetition=True)
    for restype in diff:
        label = ""
        if restype == "values_changed":
            continue
        elif restype == "type_changes":
            label = "Type changed"
        elif restype == "dictionary_item_added":
            label = "Added"
        elif restype == "dictionary_item_removed":
            label = "Removed"
        else:
            label = restype
        for result in diff[restype]:
            print(label + ": " + format_key(result))

    # Procura alterações em entradas programáticas '{}'
    if "values_changed" in diff:
        for i in diff["values_changed"]:
            label = "Alteração significativa"
            new = re.findall('{(.+?)}', diff["values_changed"][i]["new_value"])
            old = re.findall('{(.+?)}', diff["values_changed"][i]["old_value"])
            if new == [] and old == []:
                # Se ambos forem vazios não faça nada
                continue
            elif (new != [] and old == []) or (new == [] and old != []):
                # Se um for vazio e o outro não
                print(label + ": " + format_key(i))
                print(new, old)
            elif set(new) != set(old):
                # Se as listas tiverem qualquer valor ou quantidade diferente
                # A ordem não é relevante
                print(label + ": " + format_key(i))
                print(new, old)

if __name__ == "__main__":
    main()
    input("Pressiona qualquer tecla para sair.")