Foundry VTT Archmage (13th Age Compatible) Brazilian Portuguese
===============================================================


## Português

**NOTA**: Tradução em andamento.

Esse módulo adiciona o idioma Português (Brasil) como uma opção a ser selecionada nas configurações do FoundryVTT. Selecionar essa opção traduzirá a ficha de personagem do sistema Archmage (Compatível com 13ª Era) e seus compêndios.

Esse módulo traduz somente aspectos relacionados ao sistema [Archmage](https://gitlab.com/asacolips-projects/foundry-mods/archmage). Ele não traduz outras partes do software Foundry VTT, como a interface principal. Para isso, confira o módulo [Brazillian Portuguese Core](https://foundryvtt.com/packages/ptBR-core/).

Para a tradução dos Compêndios, é necessário que seja instalado o módulo [Babele](https://foundryvtt.com/packages/babele/).


### Instalação

A tradução está disponível na lista de Módulos Complementares para instalar com o nome de Translation: 13ª Era - Português (Brasil).

Lembre-se de também instalar o módulo [Babele](https://foundryvtt.com/packages/babele/) para a tradução dos Compêndios.

#### Instalação por Manifesto

Na opção `Add-On Modules` clique em `Install Module` e coloque o seguinte link no campo `Manifest URL`

`https://gitlab.com/fvtt-brasil/13th-age/-/raw/main/module.json`

<!-- #### Instalação Manual (Não recomendado)

Se as opções acima não funcionarem, visite a página com os [pacotes](https://gitlab.com/fvtt-brasil/core/-/packages), clique na versão que você deseja usar, faça o download do arquivo `ose-ptbr.zip` e extraia o conteúdo na pasta `Data/modules/`.

Feito isso ative o módulo nas configurações do mundo em que pretende usá-lo e depois altere o idioma nas configurações. -->


## Agradecimentos

- [Asacolips Projects / Foundry Mods / Toolkit13 - 13th Age Compatible · GitLab](https://gitlab.com/asacolips-projects/foundry-mods/archmage)
- [mhilbrunner/toolkit13-de: German translation for the Toolkit13 (13th Age compatible) system for Foundry Virtual Tabletop](https://github.com/mhilbrunner/toolkit13-de)
