Hooks.once('init', () => { 
	if(typeof Babele !== 'undefined') { 
		Babele.get().register({
			module: '13era-ptbr',
			lang: 'pt-BR',
			dir: 'compendium'
		}); 
	};
});

// Conversores
Hooks.once('init', () => { 
	// Babele.get().registerConverters({
		// "lbToKg": (value) => {
	// 		return parseInt(value) / 2
	// });
	// https://gitlab.com/riccisi/foundryvtt-babele/-/issues/41
	// https://gitlab.com/riccisi/foundryvtt-babele/-/issues/42

	// Babele.get().registerConverters({
	// 	"teste": (value, translation) => {
	// 		if (translation) {
	// 			var uniao = translation + " alow";
	// 			return uniao;
	// 		} else {
	// 			let newValue = value.replace("AC","CA");
	// 			// var newValue = value + " teste";
	// 			return newValue;
	// 		}
	// 	}
	// });

	Babele.get().registerConverters({
		"attack": (value, translation) => {
			if (translation) {
				let mudar = translation.replace("+@std","+@lvl+@ed");
				return mudar;
			} else {
				let mudar = value.replace("+@std","+@lvl+@ed");
				mudar = mudar.replace("AC","CA");
				mudar = mudar.replace("PD","DF");
				mudar = mudar.replace("MD","DM");
				return mudar;
			}
		}
	});

	Babele.get().registerConverters({
		"damage": (value, translation) => {
			if (translation) {
				return translation;
			} else {
				let mudar = value.replace("damage","dano");
				return mudar;
			}
		}
	});

	Babele.get().registerConverters({
		"target": (value, translation) => {
			if (translation) {
				return translation;
			} else {
				let mudar = value.replace("One enemy","Um inimigo");
				return mudar;
			}
		}
	});

	Babele.get().registerConverters({
		"range": (value, translation) => {
			if (translation) {
				return translation;
			} else {
				let mudar = value.replace("Close-quarters spell","Magia de curta distância");
				mudar = mudar.replace("Ranged spell","Magia de longa distância");
				mudar = mudar.replace("Melee attack","Ataque Corpo a Corpo");
				mudar = mudar.replace("Ranged","Ataque à Distância");
				return mudar;
			}
		}
	});

// CONVERSORES DO NOME DA CLASSE

	Babele.get().registerConverters({
		"powerSourceNameBarbaro": (value) => {
			let mudar = value.replace("Barbarian","Bárbaro");
			return mudar;
		}
	});

	Babele.get().registerConverters({
		"powerSourceNameLadino": (value) => {
			let mudar = value.replace("Rogue","Ladino");
			return mudar;
		}
	});

	Babele.get().registerConverters({
		"powerSourceNameClerigo": (value) => {
			let mudar = value.replace("Cleric","Clérigo");
			return mudar;
		}
	});

	Babele.get().registerConverters({
		"translateNpcItems": Converters.fromPack({
			"description": "system.description.value",
			"attack": {
				"path": "system.attack.value",
				"converter": "attack"
			},
			"hit": {
				"path": "system.hit.value",
				"converter": "damage"
			},
			"hit1-name": "system.hit1.name",
			"hit1-value": "system.hit1.value",
			"miss": {
				"path": "system.miss.value",
				"converter": "damage"
			}
		})
	});

});
